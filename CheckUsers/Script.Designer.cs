﻿namespace CheckUsers
{
    partial class Script
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtURLtextfile = new System.Windows.Forms.TextBox();
            this.txtURLcsvfile = new System.Windows.Forms.TextBox();
            this.BrowseTxt = new System.Windows.Forms.Button();
            this.BrowseCsv = new System.Windows.Forms.Button();
            this.btnGenerateAndSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(36, 70);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(84, 23);
            this.btnGenerate.TabIndex = 0;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtURLtextfile
            // 
            this.txtURLtextfile.Location = new System.Drawing.Point(12, 12);
            this.txtURLtextfile.Name = "txtURLtextfile";
            this.txtURLtextfile.ReadOnly = true;
            this.txtURLtextfile.Size = new System.Drawing.Size(196, 23);
            this.txtURLtextfile.TabIndex = 1;
            this.txtURLtextfile.Text = "Choose txt file";
            // 
            // txtURLcsvfile
            // 
            this.txtURLcsvfile.Location = new System.Drawing.Point(12, 41);
            this.txtURLcsvfile.Name = "txtURLcsvfile";
            this.txtURLcsvfile.ReadOnly = true;
            this.txtURLcsvfile.Size = new System.Drawing.Size(196, 23);
            this.txtURLcsvfile.TabIndex = 2;
            this.txtURLcsvfile.Text = "Choose csv file";
            // 
            // BrowseTxt
            // 
            this.BrowseTxt.Location = new System.Drawing.Point(214, 11);
            this.BrowseTxt.Name = "BrowseTxt";
            this.BrowseTxt.Size = new System.Drawing.Size(75, 23);
            this.BrowseTxt.TabIndex = 3;
            this.BrowseTxt.Text = "Browse txt";
            this.BrowseTxt.UseVisualStyleBackColor = true;
            this.BrowseTxt.Click += new System.EventHandler(this.BrowseTxt_Click);
            // 
            // BrowseCsv
            // 
            this.BrowseCsv.Location = new System.Drawing.Point(214, 41);
            this.BrowseCsv.Name = "BrowseCsv";
            this.BrowseCsv.Size = new System.Drawing.Size(75, 23);
            this.BrowseCsv.TabIndex = 4;
            this.BrowseCsv.Text = "Browse csv";
            this.BrowseCsv.UseVisualStyleBackColor = true;
            this.BrowseCsv.Click += new System.EventHandler(this.BrowseCsv_Click);
            // 
            // btnGenerateAndSave
            // 
            this.btnGenerateAndSave.Location = new System.Drawing.Point(126, 70);
            this.btnGenerateAndSave.Name = "btnGenerateAndSave";
            this.btnGenerateAndSave.Size = new System.Drawing.Size(143, 23);
            this.btnGenerateAndSave.TabIndex = 5;
            this.btnGenerateAndSave.Text = "Generate and save";
            this.btnGenerateAndSave.UseVisualStyleBackColor = true;
            this.btnGenerateAndSave.Click += new System.EventHandler(this.btnGenerateAndSave_Click);
            // 
            // Script
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 100);
            this.Controls.Add(this.btnGenerateAndSave);
            this.Controls.Add(this.BrowseCsv);
            this.Controls.Add(this.BrowseTxt);
            this.Controls.Add(this.txtURLcsvfile);
            this.Controls.Add(this.txtURLtextfile);
            this.Controls.Add(this.btnGenerate);
            this.Name = "Script";
            this.Text = "Script App";
            this.Load += new System.EventHandler(this.Script_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtURLtextfile;
        private System.Windows.Forms.TextBox txtURLcsvfile;
        private System.Windows.Forms.Button BrowseTxt;
        private System.Windows.Forms.Button BrowseCsv;
        private System.Windows.Forms.Button btnGenerateAndSave;
    }
}

