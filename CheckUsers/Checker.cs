﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CheckUsers
{
    class Checker
    {
        private int _deletedRowCount = 0;
        private string _txtPath = string.Empty;
        public string TxtPath
        {
            set { _txtPath = value; }
        }
        private string _csvPath = string.Empty;
        public string CsvPath
        {
            set { _csvPath = value; }
        }
        public Checker()
        {
        }

        public List<string> GetUsersFromTxt()
        {
            var list = new List<string>();
            var txtText = File.ReadAllText(_txtPath);
            var tempStr = txtText.Split(new string[] { "INFO:","\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 1; i < tempStr.Length; i+=2)
            {
                list.Add(tempStr[i]);
            }
            return list;
        }

        public void GetUsersWithoutTxtFile(bool isSave = false)
        {
            var txtUsers = GetUsersFromTxt();
            var csvText = File.ReadAllText(_csvPath);
            var tempStr = csvText.Split("\r\n");
            foreach (var userTxt in txtUsers)
            {
                for (int i = 0; i < tempStr.Length; ++i)
                {
                    if (tempStr[i].Contains(userTxt))
                    {
                        tempStr[i] = string.Empty;
                        _deletedRowCount++;
                    }
                }
            }
            var fileStringFinal = string.Join("\r\n", string.Join("\r\n", tempStr).Split("\r\n", StringSplitOptions.RemoveEmptyEntries)); //remove empty rows
            if (isSave)
            {
                var dir = string.Empty;
                using (var fbd = new FolderBrowserDialog())
                {
                    var result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                        dir = $@"{fbd.SelectedPath}\UsersScript_{DateTime.Now:yyyy-MM-dd-H-mm-ss}.csv";
                }
                File.WriteAllText(dir, fileStringFinal);
                MessageBox.Show($"Deleted {_deletedRowCount} row and saved: {dir}");
            }
            else
            {
                Clipboard.SetText(fileStringFinal);
                MessageBox.Show($"Deleted {_deletedRowCount} row and copied to clipboard");
            }
        }
    }
}
