﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckUsers
{
    public partial class Script : Form
    {
        Checker checker = new Checker();
        public Script()
        {
            InitializeComponent();
        }

        private void Script_Load(object sender, EventArgs e)
        {
            
        }
        private void BrowseTxt_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new OpenFileDialog())
            {
                fileDialog.Title = "Browse Text Files";
                fileDialog.DefaultExt = "txt";
                fileDialog.Filter = "txt files (*.txt)|*.txt";

                if (fileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fileDialog.FileName))
                {
                    txtURLtextfile.Text = fileDialog.FileName;
                    checker.TxtPath = fileDialog.FileName;
                }
            }
        }

        private void BrowseCsv_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new OpenFileDialog())
            {
                fileDialog.Title = "Browse Text Files";
                fileDialog.DefaultExt = "csv";
                fileDialog.Filter = "csv files (*.csv)|*.csv";

                if (fileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fileDialog.FileName))
                {
                    txtURLcsvfile.Text = fileDialog.FileName;
                    checker.CsvPath = fileDialog.FileName;
                }
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if(PathChecker())
                checker.GetUsersWithoutTxtFile();
        }

        private void btnGenerateAndSave_Click(object sender, EventArgs e)
        {
            if(PathChecker())
                checker.GetUsersWithoutTxtFile(true);
        }
        private bool PathChecker()
        {
            if(txtURLtextfile.Text == "Choose txt file")
            {
                MessageBox.Show(this,"Choose txt file");
                return false;
            }
            if (txtURLcsvfile.Text == "Choose csv file")
            {
                MessageBox.Show(this,"Choose csv file");
                return false;
            }
            return true;
        }

    }
}
